export const formatDouble = (double) =>
  double.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ',');

export const formatInteger = (integer) =>
  integer.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
