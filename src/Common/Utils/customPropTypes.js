// move all custom Proptypes to this file
const requiredError =
  (propFullName, componentName, value) => `The prop \`${propFullName}\` is marked as required in \`${componentName}\`, but its value is \`${value}\``;

export const CustomPropTypes = {
  requiredIf: (field, value) => (props, propName, componentName, location, propFullName) => {
    if (props[field] === value && (props[propName] === undefined)) {
      return new Error(requiredError(propFullName, componentName, props[propName]));
    }
  },

  minSize: (name, min) => (props, propName, componentName, location, propFullName) => {
    if (props[name].length < min) {
      return new Error(`The prop \`${propFullName}\` should have min size ${min} in \`${componentName}\`. Size: ${props[name].length}`);
    }
  },
};
