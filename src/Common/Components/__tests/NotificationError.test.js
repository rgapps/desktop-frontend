import React from 'react';
import {mount} from 'enzyme';
import '../../../__tests/setupTests';
import NotificationError from '../NotificationError';

describe('<NotificationError/>', () => {
  it('renders with title', () => {
    const wrapper = mount(<NotificationError title={'error'}/>);

    expect(wrapper.find('strong').text()).toBe('error');
    expect(wrapper.text()).toBe('error');
  });

  it('renders with title and errors', () => {
    const wrapper = mount(<NotificationError title={'error'} errors={['error1', 'error2']}/>);

    expect(wrapper.find('strong').text()).toBe('error');
    expect(wrapper.text()).toContain('error1');
    expect(wrapper.text()).toContain('error2');
  });

  it('hide if clicked', () => {
    const wrapper = mount(<NotificationError title={'error'} errors={['error1', 'error2']}/>);

    expect(wrapper.find('strong').text()).toBe('error');
    expect(wrapper.text()).toContain('error1');
    expect(wrapper.text()).toContain('error2');

    wrapper.simulate('click');

    expect(wrapper.text()).toBe('');
  });
});
