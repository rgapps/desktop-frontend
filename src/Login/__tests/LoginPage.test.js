import React from 'react';
import {mount, shallow} from 'enzyme';
import '../../__tests/setupTests';
import LoginPage from '../LoginPage';
import Login from '../Login';
import {mockedServer, rest, waitExpectWithAct} from '../../__tests/setupTests';
import {Endpoints} from '../../endpoints';
import LockingError from '../../Common/Components/LockingError';
import NotificationError from '../../Common/Components/NotificationError';

describe('<LoginPage />', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<LoginPage/>);

    const desk = wrapper.find(Login);
    expect(desk.exists()).toBeTruthy();
  });

  it('Should be displayed correctly', () => {
    const wrapper = mount(
      <LoginPage/>,
    );

    const inputs = wrapper.update().find('input');
    expect(inputs).toHaveLength(2);
    // TODO: Add check of the input labels
    expect(inputs.at(0).prop('value')).toBe(''); // User
    expect(inputs.at(1).prop('value')).toBe(''); // Password

    expect(wrapper.find('button').at(0).text()).toBe('Login');
  });

  it('Should setToken when logged in ', async () => {
    mockedServer.use(rest.post(Endpoints.logIn(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json({'token': 'myBearerToken'}));
    }));

    const setTokenCallback = jest.fn();

    const wrapper = mount(<LoginPage setToken={setTokenCallback}/>);

    wrapper.find('input').at(0).simulate('change', {target: {value: 'user'}}); // User
    wrapper.find('input').at(1).simulate('change', {target: {value: 'password'}}); // Password
    const loginButton = wrapper.find('button').at(0);
    loginButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(1); // login
      wrapper.update();
      expect(setTokenCallback).toBeCalledTimes(1);
      expect(setTokenCallback).toBeCalledWith({'token': 'myBearerToken'});
    });
  });

  it('Should fail if error on login in', async () => {
    mockedServer.use(rest.post(Endpoints.logIn(), (req, res, ctx) => {
      return res.networkError('error message');
    }));

    const setTokenCallback = jest.fn();

    const wrapper = mount(<LoginPage setToken={setTokenCallback}/>);

    wrapper.find('input').at(0).simulate('change', {target: {value: 'user'}}); // User
    wrapper.find('input').at(1).simulate('change', {target: {value: 'password'}}); // Password
    const loginButton = wrapper.find('button').at(0);
    loginButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(1); // login
      wrapper.update();
      expect(setTokenCallback).toBeCalledTimes(0);
      expect(wrapper.find(LockingError).text()).toContain('Network Error');
      expect(wrapper.find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should fail with invalid authentication if 401 error on login in', async () => {
    window.location.pathname = Endpoints.logInPage();
    mockedServer.use(rest.post(Endpoints.logIn(), (req, res, ctx) => {
      return res(ctx.status(401), ctx.json({message: 'login Error', errors: []}));
    }));

    const setTokenCallback = jest.fn();

    const wrapper = mount(<LoginPage setToken={setTokenCallback}/>);

    wrapper.find('input').at(0).simulate('change', {target: {value: 'user'}}); // User
    wrapper.find('input').at(1).simulate('change', {target: {value: 'password'}}); // Password
    const loginButton = wrapper.find('button').at(0);
    loginButton.simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(1); // login
      wrapper.update();
      expect(setTokenCallback).toBeCalledTimes(0);
      expect(wrapper.update().find(NotificationError).text()).toContain('login Error');
    });
  });
});
