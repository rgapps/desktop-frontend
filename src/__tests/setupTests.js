import {format} from 'util';
import * as Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {act} from '@testing-library/react';
import waitForExpect from 'wait-for-expect';
import {setupServer} from 'msw/node';
import {rest} from 'msw';
import './__mocks/window-location-mock';
import './__mocks/session-storage-mock';
import './__mocks/alert-mock';
import './__mocks/react-router-dom-mock';
import mediaQuery from 'css-mediaquery';
import {emptyFunction} from '../Common/Utils/dumbUtils';

// this is to make low resolution hidden items visible for tests
function createMatchMedia(width) {
  return (query) => ({
    matches: mediaQuery.match(query, {width}),
    addListener: emptyFunction,
    removeListener: emptyFunction,
  });
}

beforeAll(() => {
  window.matchMedia = createMatchMedia(window.innerWidth);
});

Enzyme.configure({adapter: new Adapter()});

const error = global.console.error;
// Solution to fail when react Warning comes https://github.com/facebook/jest/issues/6121
global.console.error = (...args) => {
  error(...args);
  throw new Error(format(...args));
};

jest.setTimeout(10000);

// TODO: Find a way to test animations using fake times (Will make tests faster)
// also, next method should be  replaced with  wait method from react tests. (tried but didn't work)
const waitExpectWithAct = (expecting) => act(() => {
  return waitForExpect(expecting, 1000);
});

const expectToThrow = (func, error) => {
  // Even though the error is caught, it still gets printed to the console
  // so we mock that out to avoid the wall of red text.
  jest.spyOn(global.console, 'error').mockImplementation((...args) => {
    throw new Error(format(...args));
  });

  expect(func).toThrowError(error);

  global.console.error.mockRestore();
};

const sleep = (time) => act(() => new Promise((r) => setTimeout(r, time)));

// server mocking
// TODO: separate this logic into a separate mock configuration file
const mockedServer = setupServer();

// inject variables to allow mock assertion
const mockStatus = {failed: false, configuredMocks: [], calls: [], unhandled: [], receivedCallsAsserted: false};
const originalUse = mockedServer.use;
mockedServer.use = (handler) => {
  const originalResolver = handler.resolver;
  mockStatus.configuredMocks.push({mask: handler.getMetaInfo().mask});
  handler.resolver = (req, res, ctx) => {
    mockStatus.receivedCallsAsserted = false;
    mockStatus.calls.push({mask: handler.getMetaInfo().mask});
    return originalResolver(req, res, ctx);
  };
  originalUse(handler);
};
//* TODO: update msw and use msw-expect to do validations *//
mockedServer['receivedCalls'] = () => {
  mockStatus.receivedCallsAsserted = true;
  return mockStatus.calls.length;
};

beforeAll(() =>
  mockedServer.listen({
    onUnhandledRequest: (req) => {
      mockStatus.failed = true;
      mockStatus.unhandled.push({method: req.method, url: req.url.href});
    },
  }));
beforeEach(() => {
  mockedServer.resetHandlers();
  mockStatus.failed = false;
  mockStatus.configuredMocks = [];
  mockStatus.calls = [];
  mockStatus.unhandled = [];
  mockStatus.receivedCallsAsserted = false;
});
afterEach(() => {
  if (mockStatus.configuredMocks.length > 0 && !mockStatus.receivedCallsAsserted) {
    console.error(`No 'receivedCalls' assertion for calls: (${mockStatus.calls.map((it) => JSON.stringify(it))}) 
      Asserting the amount of received calls is mandatory since UI can be updated when a request is finished and such changes should be tested`);
  }
  if (mockStatus.failed) {
    console.error(`Found unhandled requests: ${mockStatus.unhandled.map((it) => JSON.stringify(it))}
    Actual configuredMocks: ${mockStatus.configuredMocks.map((it) => JSON.stringify(it))}`);
  }
});
afterAll(() => {
  mockedServer.close();
});

export {waitExpectWithAct, expectToThrow, sleep, mockedServer, rest};
