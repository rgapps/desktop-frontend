// mock window location to test window.location.assign calls
delete window.location;
delete window.history;

window.history = {
  back: jest.fn(),
};

window.location = {
  href: '',
  hostname: '',
  pathname: '',
  protocol: '',
  assign: jest.fn(),
  reload: jest.fn(),
};

beforeEach(() => {
  window.location.href = '';
  window.location.hostname = '';
  window.location.pathname = '';
  window.location.protocol = '';
  window.location.assign.mockClear();
  window.location.reload.mockClear();
  window.history.back.mockClear();
});
