// mock window sessionStorage to test window.sessionStorage calls
delete window.sessionStorage;
window.sessionStorage = {
  getItem: jest.fn(),
  setItem: jest.fn(),
  clear: jest.fn(),
};

beforeEach(() => {
  window.sessionStorage.getItem.mockClear();
  window.sessionStorage.setItem.mockClear();
  window.sessionStorage.clear.mockClear();
});
