import React from 'react';
import Piece from '../Piece';
import {shallow} from 'enzyme';
import {act} from '@testing-library/react';
import '../../../../__tests/setupTests';
import 'jest-styled-components';

describe('<Piece />', () => {
  it('Should set isDragging to true when onDragStart', async () => {
    const piece = {type: 'p', color: 'b'};
    const wrapper = shallow(<Piece piece={piece} position={'a1'}/>);
    const setData = jest.fn();
    const setDragImage = jest.fn();
    const dragStartEvt = {
      dataTransfer: {setData: setData, effectAllowed: 'move', setDragImage: setDragImage},
      target: {
        id: 'a8',
      },
    };
    await act(async () => wrapper.prop('onDragStart')(dragStartEvt));
    expect(wrapper.prop('isDragging')).toBe(true);
  });

  it('Should set isDragging to false when onDragEnd', async () => {
    const piece = {type: 'p', color: 'b'};
    const wrapper = shallow(<Piece piece={piece} position={'a1'}/>);

    await act(async () => wrapper.prop('onDragEnd')());
    expect(wrapper.prop('isDragging')).toBe(false);
  });

  it('Should not be draggable if not piece', () => {
    const wrapper = shallow(<Piece piece={null} position={'a1'}/>);

    expect(wrapper.prop('draggable')).toBe(false);
  });

  it('Should not be visible if dragging', async () => {
    const piece = {type: 'p', color: 'b'};
    const wrapper = shallow(<Piece piece={piece} position={'a1'}/>);

    const setData = jest.fn();
    const setDragImage = jest.fn();
    const dragStartEvt = {
      dataTransfer: {setData: setData, effectAllowed: 'move', setDragImage: setDragImage},
      target: {
        id: 'a8',
      },
    };

    await act(async () => wrapper.prop('onDragStart')(dragStartEvt));
    expect(wrapper).toHaveStyleRule('opacity', '0');
  });

  it('Should be visible if not dragging', () => {
    const piece = {type: 'p', color: 'b'};
    const wrapper = shallow(<Piece piece={piece} position={'a1'}/>);

    expect(wrapper).toHaveStyleRule('opacity', '1');
  });
});
