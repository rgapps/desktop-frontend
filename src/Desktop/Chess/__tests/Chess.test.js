import React from 'react';
import Chess from '../Chess';
import {mount} from 'enzyme';
import {waitExpectWithAct} from '../../../__tests/setupTests';
import {act} from '@testing-library/react';
import '../../../__tests/setupTests';
import 'jest-styled-components';
import {AnimationType, defaultAnimationBehavior} from '../../Animations/Behavior/behaviorDefinitions';
import {AnimationBehaviorContext} from '../../Animations/Behavior/AnimationBehaviorHandler';

describe('<Chess/>', () => {
  it('Should redirect to ChessPage', async () => {
    const wrapper = mount(<Chess/>);
    wrapper.simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith(`/chess`);
    });
  });

  it('dont redirect to desktop if Draggable animation is running', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      runningAny: (type) => type === AnimationType.Drag,
    };
    const wrapper = mount(<AnimationBehaviorContext.Provider value={animationBehavior}>
      <Chess/>
    </AnimationBehaviorContext.Provider>,
    );
    await act(async () => {
      wrapper.find(Chess).simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(window.location.assign).not.toBeCalled();
    });
  });
});
