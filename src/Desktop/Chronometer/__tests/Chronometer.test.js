import React from 'react';
import Chronometer from '../Chronometer';
import {sleep, waitExpectWithAct} from '../../../__tests/setupTests';
import {mount, shallow} from 'enzyme';


describe('<Chronometer />', () => {
  it('Should have three buttons', () => {
    const wrapper = shallow(<Chronometer/>);
    const button = wrapper.children();

    expect(button.length).toBe(4);
    expect(button.at(1).prop('action')).toBe('restart');
    expect(button.at(2).prop('action')).toBe('stop');
    expect(button.at(3).prop('action')).toBe('start');
  });

  it('Should start with 00:00:00/000', () => {
    const wrapper = shallow(<Chronometer/>);
    const time = wrapper.children().at(0);

    expect(time.children().length).toBe(2);
    expect(time.children().at(0).text()).toEqual('00:00:00');
    expect(time.children().at(1).text()).toEqual('000');
  });

  it('Button start should work', async () => {
    const wrapper = mount(<Chronometer/>);
    const time = wrapper.find('.milis');
    const buttonStart = wrapper.find('#buttonStart');

    expect(time.text()).toBe('000');
    buttonStart.at(1).simulate('click');
    await waitExpectWithAct(() => {
      expect(parseInt(time.text())).toBeGreaterThan(100);
    });
  });

  it('Button stop should work', async () => {
    const wrapper = mount(<Chronometer/>);
    const time = wrapper.find('.milis');
    const buttonStart = wrapper.find('#buttonStart');
    const buttonStop = wrapper.find('#buttonStop');

    expect(time.text()).toBe('000');
    buttonStart.at(1).simulate('click');
    await waitExpectWithAct(() => {
      expect(parseInt(time.text())).toBeGreaterThan(100);
    });
    buttonStop.at(1).simulate('click');
    const stopTime = time.text();
    expect(parseInt(stopTime)).toBeGreaterThan(100);
    await sleep(100);
    await waitExpectWithAct(() => {
      expect(time.text()).toBe(stopTime);
    });
  });

  it('Button start continues after stopping', async () => {
    const wrapper = mount(<Chronometer/>);
    const time = wrapper.find('.milis');
    const buttonStart = wrapper.find('#buttonStart');
    const buttonStop = wrapper.find('#buttonStop');

    expect(time.text()).toBe('000');
    buttonStart.at(1).simulate('click');
    await waitExpectWithAct(() => {
      expect(parseInt(time.text())).toBeGreaterThan(100);
    });
    buttonStop.at(1).simulate('click');
    const stopTime = parseInt(time.text());
    await sleep(100);
    await waitExpectWithAct(() => {
      expect(parseInt(time.text())).toBe(stopTime);
    });
    buttonStart.at(1).simulate('click');
    expect(parseInt(time.text())).toBe(stopTime);
    await waitExpectWithAct(() => {
      expect(parseInt(time.text())).toBeGreaterThan(stopTime);
    });
  });

  it('Button restart should restart and keep counting if chronometer is running', async () => {
    const wrapper = mount(<Chronometer/>);
    const time = wrapper.find('.milis');
    const buttonStart = wrapper.find('#buttonStart');
    const buttonRestart = wrapper.find('#buttonRestart');

    expect(time.text()).toBe('000');
    buttonStart.at(1).simulate('click');
    await waitExpectWithAct(() => {
      expect(parseInt(time.text())).toBeGreaterThan(100);
    });
    buttonRestart.at(1).simulate('click');
    expect(time.text()).toBe('000');
    await waitExpectWithAct(() => {
      expect(parseInt(time.text())).toBeGreaterThan(100);
    });
  });

  it('Button restart should restart and dont count if chronometer is stopped', async () => {
    const wrapper = mount(<Chronometer/>);
    const time = wrapper.find('.milis');
    const buttonStart = wrapper.find('#buttonStart');
    const buttonStop = wrapper.find('#buttonStop');
    const buttonRestart = wrapper.find('#buttonRestart');

    expect(time.text()).toBe('000');
    buttonStart.at(1).simulate('click');
    await waitExpectWithAct(() => {
      expect(parseInt(time.text())).toBeGreaterThan(100);
    });
    buttonStop.at(1).simulate('click');
    expect(parseInt(time.text())).toBeGreaterThan(100);
    buttonRestart.at(1).simulate('click');
    expect(time.text()).toBe('000');
    await sleep(100);
    await waitExpectWithAct(() => {
      expect(time.text()).toBe('000');
    });
  });

  it('Click two times in button start should not restart the count', async () => {
    const wrapper = mount(<Chronometer/>);
    const time = wrapper.find('.milis');
    const buttonStart = wrapper.find('#buttonStart');

    expect(time.text()).toBe('000');
    buttonStart.at(1).simulate('click');
    await waitExpectWithAct(() => {
      expect(parseInt(time.text())).toBeGreaterThan(100);
    });
    buttonStart.at(1).simulate('click');
    expect(parseInt(time.text())).toBeGreaterThan(0);
  });
});
