import React from 'react';
import LanguageCardsGame from '../LanguageCardsGame';
import {mount, shallow} from 'enzyme';
import {expectToThrow, waitExpectWithAct} from '../../../__tests/setupTests';
import {AnimationType, defaultAnimationBehavior} from '../../Animations/Behavior/behaviorDefinitions';
import {AnimationBehaviorContext} from '../../Animations/Behavior/AnimationBehaviorHandler';
import '../../../__tests/setupTests';
import 'jest-styled-components';
import each from 'jest-each';
import Draggable from '../../../Desktop/Animations/Draggable';
import {act} from '@testing-library/react';

describe('<LanguageCards />', () => {
  const info = {
    name: 'LanguageCards',
    language: 'English',
    cards: [
      {
        name: {
          wordType: 'dummy word 1',
          word: 'Dummy',
          example: 'dummy example 1',
          description: 'dummy description 1',
        },
      }, {
        name: {
          wordType: 'dummy word 2',
          word: 'Dummy',
          example: 'dummy example 2',
          description: 'dummy description 2',
        },
      }, {
        name: {
          wordType: 'dummy word 3',
          word: 'Dummy',
          example: 'dummy example 3',
          description: 'dummy description 3',
        },
      },
    ],
  };

  it('on dragged the element its z position is top', async () => {
    const wrapper = shallow(<LanguageCardsGame interactive={true} content={info}/>);

    expect(wrapper.find(Draggable).length).toBe(4);
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(2);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(-1);
    await act(async () => wrapper.find(Draggable).at(0).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(2);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(-1);
    await act(async () => wrapper.find(Draggable).at(1).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(2);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(-1);
    await act(async () => wrapper.find(Draggable).at(2).prop('onDragStart')());
    expect(wrapper.find(Draggable).at(0).prop('z')).toBe(0);
    expect(wrapper.find(Draggable).at(1).prop('z')).toBe(1);
    expect(wrapper.find(Draggable).at(2).prop('z')).toBe(2);
    expect(wrapper.find(Draggable).at(3).prop('z')).toBe(-1);
  });

  it('Should fails if no content', () => {
    expectToThrow(() => shallow(<LanguageCardsGame/>),
      'Failed prop type: The prop `content` is marked as required in `LanguageCardsGame`, but its value is `undefined`.');
  });

  it('Should fails if cards is empty', () => {
    const infoCards = {
      name: 'Dummy name',
      language: 'language',
      cards: [],
    };

    expectToThrow(() => shallow(<LanguageCardsGame content={infoCards}/>),
      `Warning: Failed prop type: \`LanguageCardsGame\` should contain at least one card information (0 received)`);
  });

  each([
    [{other: 'other'}, 'content.name'],
    [{name: 'name', other: 'other'}, 'content.language'],
    [{name: 'name', language: 'language', other: 'other'}, 'content.cards'],
  ]).it('Should fail if content does not match shape', (content, field) => {
    expectToThrow(() => shallow(<LanguageCardsGame content={content}/>),
      `Warning: Failed prop type: The prop \`${field}\` is marked as required in \`LanguageCardsGame\`, but its value is \`undefined\`.`);
  });

  it('should fail if it doesnt have cards[i].name', () => {
    const badCardsInfo = {
      name: 'dummyCards',
      language: 'dummyLanguage',
      cards: [{}],
    };
    expectToThrow(() => shallow(<LanguageCardsGame content={badCardsInfo}/>),
      `The prop \`content.cards[0].name\` is marked as required in \`LanguageCardsGame\`, but its value is \`undefined\`.`);
  });

  it('Should redirect to LanguageCardsPage', async () => {
    const wrapper = mount(<LanguageCardsGame content={info}/>);
    wrapper.simulate('click');
    await waitExpectWithAct(() => {
      expect(window.location.assign).toBeCalledWith(`/languageCards/${info.language}`);
    });
  });

  it('dont redirect to desktop if Draggable animation is running', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      runningAny: (type) => type === AnimationType.Drag,
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <LanguageCardsGame content={info}/>
      </AnimationBehaviorContext.Provider>,
    );
    await act(async () => {
      wrapper.find(LanguageCardsGame).simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(window.location.assign).not.toBeCalled();
    });
  });
});
