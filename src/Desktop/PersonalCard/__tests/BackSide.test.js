import React from 'react';
import {shallow} from 'enzyme';
import BackSide from '../BackSide';
import '../../../__tests/setupTests';

describe('<BackSide />', () => {
  it('renders with shared text', () => {
    const wrapper = shallow(<BackSide text={'text'}/>);

    expect(wrapper.text()).toContain('text');
  });
});
