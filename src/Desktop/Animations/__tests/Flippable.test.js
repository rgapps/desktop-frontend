import React from 'react';
import Flippable from '../Flippable';
import {mount, shallow} from 'enzyme';
import '../../../__tests/setupTests';
import {AnimationBehaviorContext} from '../Behavior/AnimationBehaviorHandler';
import {AnimationType, defaultAnimationBehavior, RunningAnimation} from '../Behavior/behaviorDefinitions';
import {expectToThrow, waitExpectWithAct} from '../../../__tests/setupTests';
import {act} from '@testing-library/react';

describe('<Flippable />', () => {
  it('Fail when no id is provided', () => {
    expectToThrow(() => shallow(
      <Flippable>
        <div>1</div>
        <div>2</div>
      </Flippable>,
    ), 'Warning: Failed prop type: The prop `id` is marked as required in `Flippable`, but its value is `undefined`');
  });

  it('Fail when more than two children are provided', () => {
    expectToThrow(() => shallow(
      <Flippable id={'flippable'}>
        <div>1</div>
        <div>2</div>
        <div>3</div>
      </Flippable>,
    ), 'Warning: Failed prop type: `Flippable` should contain at least one and maximum two children (3 received)');
  });

  it('Fail when no children are provided', () => {
    expectToThrow(() => shallow(<Flippable id={'flippable'}/>),
      'Warning: Failed prop type: `Flippable` should contain at least one and maximum two children (0 received)');
  });

  it('Renders not flipped by default', () => {
    const wrapper = mount(
      <Flippable id={'flippable'}>
        <div>front</div>
        <div>back</div>
      </Flippable>,
    );

    expect(wrapper.render().html()).toBe('<div>front</div>');
  });

  it('Renders flipped when startFlipped prop is true', () => {
    const wrapper = mount(
      <Flippable id={'flippable'} startFlipped={true}>
        <div>front</div>
        <div>back</div>
      </Flippable>,
    );

    expect(wrapper.render().html()).toBe('<div>back</div>');
  });

  it('Renders flipped with empty back if only one child is given', () => {
    const wrapper = mount(
      <Flippable id={'flippable'} startFlipped={true}>
        <div>front</div>
      </Flippable>,
    );

    expect(wrapper.render().html()).toBe('');
  });

  it('Flips when clicked (startFlipped default)', async () => {
    const wrapper = mount(
      <Flippable id={'flippable'}>
        <div>front</div>
        <div>back</div>
      </Flippable>,
    );
    expect(wrapper.render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.render().html()).toBe('<div>back</div>');
    });
  });

  it('Flips when clicked (startFlipped true)', async () => {
    const wrapper = mount(
      <Flippable id={'flippable'} startFlipped={true}>
        <div>front</div>
        <div>back</div>
      </Flippable>,
    );
    expect(wrapper.render().html()).toBe('<div>back</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.render().html()).toBe('<div>front</div>');
    });
  });

  it('Call callbacks when flipped (startFlipped default)', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const wrapper = mount(
      <Flippable id={'flippable'} onFlipStart={callbackStart} onFlipped={callbackEnd}>
        <div>front</div>
        <div>back</div>
      </Flippable>,
    );
    expect(wrapper.render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(0);
      expect(wrapper.render().html()).toBe('<div>front</div>');
    });
    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
      expect(wrapper.render().html()).toBe('<div>back</div>');
    });
  });

  it('Call callbacks when flipped (startFlipped true)', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const wrapper = mount(
      <Flippable id={'flippable'} startFlipped={true} onFlipped={callbackEnd} onFlipStart={callbackStart}>
        <div>front</div>
        <div>back</div>
      </Flippable>,
    );
    expect(wrapper.render().html()).toBe('<div>back</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(0);
      expect(wrapper.render().html()).toBe('<div>back</div>');
    });
    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
      expect(wrapper.render().html()).toBe('<div>front</div>');
    });
  });

  it('Flips again when clicked after one flip (startFlipped default)', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const wrapper = mount(
      <Flippable id={'flippable'} onFlipped={callbackEnd} onFlipStart={callbackStart}>
        <div>front</div>
        <div>back</div>
      </Flippable>,
    );
    expect(wrapper.render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.render().html()).toBe('<div>back</div>');
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
    });
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.render().html()).toBe('<div>front</div>');
      expect(callbackStart).toBeCalledTimes(2);
      expect(callbackEnd).toBeCalledTimes(2);
    });
  });

  it('Flips again when clicked after one flip (startFlipped true)', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const wrapper = mount(
      <Flippable id={'flippable'} startFlipped={true} onFlipped={callbackEnd} onFlipStart={callbackStart}>
        <div>front</div>
        <div>back</div>
      </Flippable>,
    );
    expect(wrapper.render().html()).toBe('<div>back</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.render().html()).toBe('<div>front</div>');
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
    });
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.render().html()).toBe('<div>back</div>');
      expect(callbackStart).toBeCalledTimes(2);
      expect(callbackEnd).toBeCalledTimes(2);
    });
  });

  it('Call AnimationBehaviorContext start and stop when flipped', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider
        value={animationBehavior}>
        <Flippable id={'Flippable-test'}>
          <div>front</div>
          <div>back</div>
        </Flippable>
      </AnimationBehaviorContext.Provider>,
    );
    expect(wrapper.render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Flip, 'Flippable-test'));
    expect(animationBehavior.start).toBeCalledTimes(1);
    expect(animationBehavior.stop).toBeCalledTimes(0);
    await waitExpectWithAct(() => {
      expect(wrapper.render().html()).toBe('<div>back</div>');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Flip, 'Flippable-test'));
      expect(animationBehavior.start).toBeCalledTimes(1);
      expect(animationBehavior.stop).toBeCalledWith(new RunningAnimation(AnimationType.Flip, 'Flippable-test'));
      expect(animationBehavior.stop).toBeCalledTimes(1);
    });
  });

  it('Dont flip if disabled by AnimationBehaviorContext', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      allowed: () => false,
      start: jest.fn(),
      stop: jest.fn(),
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <Flippable id={'flippable'}>
          <div>front</div>
          <div>back</div>
        </Flippable>
      </AnimationBehaviorContext.Provider>,
    );
    expect(wrapper.render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    expect(animationBehavior.start).toBeCalledTimes(0);
    expect(animationBehavior.stop).toBeCalledTimes(0);
  });

  it('Dont flip with click when external flipper is configured (startFlipped default)', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };

    const ref = React.createRef();
    mount(<div ref={ref}/>);
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <Flippable id={'flippable'} externalTrigger={ref}>
          <div>front</div>
          <div>back</div>
        </Flippable>
      </AnimationBehaviorContext.Provider>,
    );
    expect(wrapper.find(Flippable).render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });

    expect(animationBehavior.start).toBeCalledTimes(0);
    expect(animationBehavior.stop).toBeCalledTimes(0);
  });

  it('Dont flip with click when external flipper is configured starting not paged even with ref defined after Pageable', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };

    const ref = React.createRef();
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <Flippable id={'flippable'} externalTrigger={ref}>
          <div>front</div>
          <div>back</div>
        </Flippable>
      </AnimationBehaviorContext.Provider>,
    );
    mount(<div ref={ref}/>);
    expect(wrapper.find(Flippable).render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });

    expect(animationBehavior.start).toBeCalledTimes(0);
    expect(animationBehavior.stop).toBeCalledTimes(0);
  });

  it('Dont flip with click when external flipper is configured (startFlipped true)', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };

    const ref = React.createRef();
    mount(<div ref={ref}/>);
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <Flippable id={'flippable'} startFlipped={true} externalTrigger={ref}>
          <div>front</div>
          <div>back</div>
        </Flippable>
      </AnimationBehaviorContext.Provider>,
    );
    expect(wrapper.find(Flippable).render().html()).toBe('<div>back</div>');
    await act(async () => {
      wrapper.simulate('click');
    });

    expect(animationBehavior.start).toBeCalledTimes(0);
    expect(animationBehavior.stop).toBeCalledTimes(0);
  });

  it('Flips when external flipper is clicked', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const ref = React.createRef();
    mount(<div ref={ref}/>);
    const wrapper = mount(
      <Flippable id={'flippable'} onFlipped={callbackEnd} onFlipStart={callbackStart} externalTrigger={ref}>
        <div>front</div>
        <div>back</div>
      </Flippable>,
    );
    expect(wrapper.find(Flippable).render().html()).toBe('<div>front</div>');

    await act(async () => {
      return ref.current.click();
    });

    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
      expect(wrapper.find(Flippable).render().html()).toBe('<div>back</div>');
    });
  });
});
