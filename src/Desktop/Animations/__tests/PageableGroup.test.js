import React from 'react';
import Pageable from '../Pageable';
import {shallow} from 'enzyme';
import {expectToThrow} from '../../../__tests/setupTests';
import PageableGroup from '../PageableGroup';
import {act} from '@testing-library/react';

describe('<PageableGroup/>', () => {
  it('Fail when no id is provided', () => {
    expectToThrow(() => shallow(
      <PageableGroup>
        <div>front</div>
        <div>back</div>
      </PageableGroup>,
    ), 'Warning: Failed prop type: The prop `id` is marked as required in `PageableGroup`, but its value is `undefined`');
  });

  it('fail when no pageCount is provided', () => {
    expectToThrow(() => shallow(<PageableGroup id={'PageableGroup'}/>),
      'Warning: Failed prop type: The prop `pageCount` is marked as required in `PageableGroup`, but its value is `undefined`');
  });

  it('fail when no pageWidth is provided', () => {
    expectToThrow(() => shallow(<PageableGroup id={'PageableGroup'} pageCount={1}/>),
      'Warning: Failed prop type: The prop `pageWidth` is marked as required in `PageableGroup`, but its value is `undefined`');
  });

  it('fail when enough children are not provided', () => {
    expectToThrow(() => shallow(
      <PageableGroup id={'PageableGroup'} pageCount={2} pageWidth={400}>
        <>
          <div>1</div>
          <div>2</div>
        </>
      </PageableGroup>,
    ), 'Warning: Failed prop type: `PageableGroup` should contain 2 children (1 received)');
  });

  it('fail when enough children are provided but they are not Fragments', () => {
    expectToThrow(() => shallow(
      <PageableGroup id={'PageableGroup'} pageCount={2} pageWidth={400}>
        <div>
          <div>1</div>
          <div>2</div>
        </div>
        <div>
          <div>1</div>
          <div>2</div>
        </div>
      </PageableGroup>,
    ), 'Warning: Failed prop type: `PageableGroup` should contain 2 Fragments with two children each');
  });

  it('fail when enough children are provided but their children are invalid', () => {
    expectToThrow(() => shallow(
      <PageableGroup id={'PageableGroup'} pageCount={2} pageWidth={400}>
        <>
          <div>1</div>
          <div>2</div>
        </>
        <>
          <div>1</div>
        </>
      </PageableGroup>,
    ), 'Warning: Failed prop type: each children of `PageableGroup` should contain exactly 2 children (front and back)');
  });

  it('fail when invalid pageWidth is provided', () => {
    expectToThrow(() => shallow(
      <PageableGroup id={'PageableGroup'} pageCount={1} pageWidth={['']}>
        <div>1</div>
        <div>2</div>
      </PageableGroup>,
    ), 'Warning: Failed prop type: Invalid prop `pageWidth` supplied to `PageableGroup');
  });

  it('fail when pageWidth is array with size different to pageCount', () => {
    expectToThrow(() => shallow(
      <PageableGroup id={'PageableGroup'} pageCount={2} pageWidth={[1, 2, 3]}>
        <>
          <div>1</div>
          <div>2</div>
        </>
        <>
          <div>3</div>
          <div>4</div>
        </>
      </PageableGroup>,
    ), 'Warning: Failed prop type: `PageableGroup`\'s `pageWidth` should be a number or an Array of size `pageCount` (received 1,2,3)');
  });

  it('contains all pages with same width', () => {
    const wrapper = shallow(
      <PageableGroup id={'PageableGroup'} pageWidth={40} pageCount={3}>
        <>
          <div>front1</div>
          <div>back1</div>
        </>
        <>
          <div>front2</div>
          <div>back2</div>
        </>
        <>
          <div>front3</div>
          <div>back3</div>
        </>
      </PageableGroup>,
    );

    const pageables = wrapper.find(Pageable);
    expect(pageables.length).toBe(3);
    expect(pageables.at(0).prop('pageWidth')).toBe(40);
    expect(pageables.at(1).prop('pageWidth')).toBe(40);
    expect(pageables.at(2).prop('pageWidth')).toBe(40);
  });

  it('Explicit fragments are valid', () => {
    const wrapper = shallow(
      <PageableGroup id={'PageableGroup'} pageWidth={40} pageCount={3}>
        <>
          <div>front1</div>
          <div>back1</div>
        </>
        <React.Fragment>
          <div>front2</div>
          <div>back2</div>
        </React.Fragment>
        <>
          <div>front3</div>
          <div>back3</div>
        </>
      </PageableGroup>,
    );

    const pageables = wrapper.find(Pageable);
    expect(pageables.length).toBe(3);
    expect(pageables.at(0).prop('pageWidth')).toBe(40);
    expect(pageables.at(1).prop('pageWidth')).toBe(40);
    expect(pageables.at(2).prop('pageWidth')).toBe(40);
  });

  it('contains all pages with different widths', () => {
    const wrapper = shallow(
      <PageableGroup id={'PageableGroup'} pageWidth={[40, 30, 41]} pageCount={3}>
        <>
          <div>front1</div>
          <div>back1</div>
        </>
        <>
          <div>front2</div>
          <div>back2</div>
        </>
        <>
          <div>front3</div>
          <div>back3</div>
        </>
      </PageableGroup>,
    );

    const pageables = wrapper.find(Pageable);
    expect(pageables.length).toBe(3);
    expect(pageables.at(0).prop('pageWidth')).toBe(40);
    expect(pageables.at(1).prop('pageWidth')).toBe(30);
    expect(pageables.at(2).prop('pageWidth')).toBe(41);
  });

  it('contains all pages with right content', () => {
    const wrapper = shallow(
      <PageableGroup id={'PageableGroup'} pageWidth={40} pageCount={3}>
        <>
          <div>front1</div>
          <div>back1</div>
        </>
        <>
          <div>front2</div>
          <div>back2</div>
        </>
        <>
          <div>front3</div>
          <div>back3</div>
        </>
      </PageableGroup>,
    );

    const pageables = wrapper.find(Pageable);
    expect(pageables.length).toBe(3);
    expect(pageables.at(0).children().length).toBe(2);
    expect(pageables.at(0).children().at(0).html()).toBe('<div>front1</div>');
    expect(pageables.at(0).children().at(1).html()).toBe('<div>back1</div>');
    expect(pageables.at(1).children().length).toBe(2);
    expect(pageables.at(1).children().at(0).html()).toBe('<div>front2</div>');
    expect(pageables.at(1).children().at(1).html()).toBe('<div>back2</div>');
    expect(pageables.at(2).children().length).toBe(2);
    expect(pageables.at(2).children().at(0).html()).toBe('<div>front3</div>');
    expect(pageables.at(2).children().at(1).html()).toBe('<div>back3</div>');
  });

  it('start in page 0 by default', () => {
    const wrapper = shallow(
      <PageableGroup id={'PageableGroup'} pageWidth={40} pageCount={3}>
        <>
          <div>front1</div>
          <div>back1</div>
        </>
        <>
          <div>front2</div>
          <div>back2</div>
        </>
        <>
          <div>front3</div>
          <div>back3</div>
        </>
      </PageableGroup>,
    );

    const pageables = wrapper.find(Pageable);
    expect(pageables.length).toBe(3);
    expect(pageables.at(0).prop('startPaged')).toBe(false);
    expect(pageables.at(1).prop('startPaged')).toBe(false);
    expect(pageables.at(2).prop('startPaged')).toBe(false);
  });

  it('start in page specified by startingPage', () => {
    const wrapper = shallow(
      <PageableGroup id={'PageableGroup'} pageWidth={40} pageCount={3} startingPage={2}>
        <>
          <div>front1</div>
          <div>back1</div>
        </>
        <>
          <div>front2</div>
          <div>back2</div>
        </>
        <>
          <div>front3</div>
          <div>back3</div>
        </>
      </PageableGroup>,
    );

    const pageables = wrapper.find(Pageable);
    expect(pageables.length).toBe(3);
    expect(pageables.at(0).prop('startPaged')).toBe(true);
    expect(pageables.at(1).prop('startPaged')).toBe(true);
    expect(pageables.at(2).prop('startPaged')).toBe(false);
  });

  it('fail when no startingPage is greater than pageCount', () => {
    expectToThrow(() => shallow(
      <PageableGroup id={'PageableGroup'} pageCount={1} pageWidth={400} startingPage={2}>
        <>
          <div>front1</div>
          <div>back1</div>
        </>
      </PageableGroup>,
    ), 'Warning: Failed prop type: `PageableGroup`\'s `startingPage` should be lower than the `pageCount` (trying to start in 2 with 1 pages)');
  });

  it('pages should be piled vertically according to current page', async () => {
    const wrapper = shallow(
      <PageableGroup id={'PageableGroup'} pageWidth={40} pageCount={3}>
        <>
          <div>front1</div>
          <div>back1</div>
        </>
        <>
          <div>front2</div>
          <div>back2</div>
        </>
        <>
          <div>front3</div>
          <div>back3</div>
        </>
      </PageableGroup>,
    );

    expect(wrapper.find(Pageable).length).toBe(3);
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(2);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
    await act(async () => wrapper.find(Pageable).at(0).prop('onPagingStart')(true));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(3);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
    await act(async () => wrapper.find(Pageable).at(0).prop('onPaged')(true));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(0);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
    await act(async () => wrapper.find(Pageable).at(1).prop('onPagingStart')(true));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(0);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(3);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
    await act(async () => wrapper.find(Pageable).at(1).prop('onPaged')(true));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(0);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
    await act(async () => wrapper.find(Pageable).at(2).prop('onPagingStart')(true));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(0);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(3);
    await act(async () => wrapper.find(Pageable).at(2).prop('onPaged')(true));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(0);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(2);
    await act(async () => wrapper.find(Pageable).at(2).prop('onPagingStart')(false));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(0);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(3);
    await act(async () => wrapper.find(Pageable).at(2).prop('onPaged')(false));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(0);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
    await act(async () => wrapper.find(Pageable).at(1).prop('onPagingStart')(false));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(0);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(3);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
    await act(async () => wrapper.find(Pageable).at(1).prop('onPaged')(false));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(0);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
    await act(async () => wrapper.find(Pageable).at(0).prop('onPagingStart')(false));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(3);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
    await act(async () => wrapper.find(Pageable).at(0).prop('onPaged')(false));
    expect(wrapper.find(Pageable).at(0).parent().prop('z')).toBe(2);
    expect(wrapper.find(Pageable).at(1).parent().prop('z')).toBe(1);
    expect(wrapper.find(Pageable).at(2).parent().prop('z')).toBe(0);
  });

  it('pages should send onPaged event according to current page', async () => {
    const callbackOnPaged = jest.fn();

    const wrapper = shallow(
      <PageableGroup id={'PageableGroup'} pageWidth={40} pageCount={3} onPaged={callbackOnPaged}>
        <>
          <div>front1</div>
          <div>back1</div>
        </>
        <>
          <div>front2</div>
          <div>back2</div>
        </>
        <>
          <div>front3</div>
          <div>back3</div>
        </>
      </PageableGroup>,
    );

    expect(wrapper.find(Pageable).length).toBe(3);
    await act(async () => wrapper.find(Pageable).at(0).prop('onPagingStart')(true));
    expect(callbackOnPaged).toBeCalledTimes(0);
    await act(async () => wrapper.find(Pageable).at(0).prop('onPaged')(true));
    expect(callbackOnPaged).toBeCalledTimes(1);
    expect(callbackOnPaged).lastCalledWith(1);
    await act(async () => wrapper.find(Pageable).at(1).prop('onPagingStart')(true));
    expect(callbackOnPaged).toBeCalledTimes(1);
    await act(async () => wrapper.find(Pageable).at(1).prop('onPaged')(true));
    expect(callbackOnPaged).toBeCalledTimes(2);
    expect(callbackOnPaged).lastCalledWith(2);
    await act(async () => wrapper.find(Pageable).at(2).prop('onPagingStart')(true));
    expect(callbackOnPaged).toBeCalledTimes(2);
    await act(async () => wrapper.find(Pageable).at(2).prop('onPaged')(true));
    expect(callbackOnPaged).toBeCalledTimes(3);
    expect(callbackOnPaged).lastCalledWith(3);
    await act(async () => wrapper.find(Pageable).at(2).prop('onPagingStart')(false));
    expect(callbackOnPaged).toBeCalledTimes(3);
    await act(async () => wrapper.find(Pageable).at(2).prop('onPaged')(false));
    expect(callbackOnPaged).toBeCalledTimes(4);
    expect(callbackOnPaged).lastCalledWith(2);
    await act(async () => wrapper.find(Pageable).at(1).prop('onPagingStart')(false));
    expect(callbackOnPaged).toBeCalledTimes(4);
    await act(async () => wrapper.find(Pageable).at(1).prop('onPaged')(false));
    expect(callbackOnPaged).toBeCalledTimes(5);
    expect(callbackOnPaged).lastCalledWith(1);
    await act(async () => wrapper.find(Pageable).at(0).prop('onPagingStart')(false));
    expect(callbackOnPaged).toBeCalledTimes(5);
    await act(async () => wrapper.find(Pageable).at(0).prop('onPaged')(false));
    expect(callbackOnPaged).toBeCalledTimes(6);
    expect(callbackOnPaged).lastCalledWith(0);
  });
});
