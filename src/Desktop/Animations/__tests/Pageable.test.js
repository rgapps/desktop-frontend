import React from 'react';
import Pageable from '../Pageable';
import {mount, shallow} from 'enzyme';
import '../../../__tests/setupTests';
import {AnimationBehaviorContext} from '../Behavior/AnimationBehaviorHandler';
import {AnimationType, defaultAnimationBehavior, RunningAnimation} from '../Behavior/behaviorDefinitions';
import {expectToThrow, waitExpectWithAct} from '../../../__tests/setupTests';
import {act} from '@testing-library/react';
import {Floating} from '../../../Common/Components/Floating';
import Flippable from '../Flippable';

describe('<Pageable />', () => {
  it('Fail when no id is provided', () => {
    expectToThrow(() => shallow(
      <Pageable>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    ), 'Warning: Failed prop type: The prop `id` is marked as required in `Pageable`, but its value is `undefined`');
  });

  it('fail when no pageWidth is provided', () => {
    expectToThrow(() => shallow(
      <Pageable id={'Pageable'}>
        <div>1</div>
        <div>2</div>
      </Pageable>,
    ), 'Warning: Failed prop type: The prop `pageWidth` is marked as required in `Pageable`, but its value is `undefined`.');
  });

  it('fail when more than two children are provided', () => {
    expectToThrow(() => shallow(
      <Pageable id={'Pageable'} pageWidth={100}>
        <div>1</div>
        <div>2</div>
        <div>3</div>
      </Pageable>,
    ), 'Warning: Failed prop type: `Pageable` should contain at least one and maximum two children (3 received)');
  });

  it('fail when no children are provided', () => {
    expectToThrow(() => shallow(<Pageable id={'Pageable'} pageWidth={100}/>),
      'Warning: Failed prop type: `Pageable` should contain at least one and maximum two children (0 received)');
  });

  it('renders not paged by default', () => {
    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40}>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    );

    expect(wrapper.find(Floating).render().prop('x')).toBe('100');
    expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
  });

  it('Renders paged when startPaged prop is true', () => {
    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40} startPaged={true}>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('0');
    expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
  });

  it('Renders paged with empty back if only one child is given', () => {
    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40} startPaged={true}>
        <div>front</div>
      </Pageable>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('0');
    expect(wrapper.find(Floating).render().html()).toBe('');
  });

  it('Turn page when clicked starting not paged', async () => {
    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40}>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('100');
    expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.find(Floating).render().prop('x')).toBe('0');
      expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
    });
  });

  it('Turn page when clicked starting paged', async () => {
    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40} startPaged={true}>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('0');
    expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.find(Floating).render().prop('x')).toBe('100');
      expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    });
  });

  it('Calls callback when paged starting not paged', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40} onPaged={callbackEnd} onPagingStart={callbackStart}>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('100');
    expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(0);
      expect(wrapper.find(Floating).render().prop('x')).toBe('100');
      expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    });
    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
      expect(wrapper.find(Floating).render().prop('x')).toBe('0');
      expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
    });
  });

  it('Calls callback when paged starting paged', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40} startPaged={true} onPaged={callbackEnd} onPagingStart={callbackStart}>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('0');
    expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(0);
      expect(wrapper.find(Floating).render().prop('x')).toBe('0');
      expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
    });
    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
      expect(wrapper.find(Floating).render().prop('x')).toBe('100');
      expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    });
  });

  it('Turn page again when clicked after one page starting not paged', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40} onPaged={callbackEnd} onPagingStart={callbackStart}>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('100');
    expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.find(Floating).render().prop('x')).toBe('0');
      expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
    });
    await act(async () => {
      wrapper.simulate('click');
    });
    await waitExpectWithAct(() => {
      expect(wrapper.find(Floating).render().prop('x')).toBe('100');
      expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
      expect(callbackStart).toBeCalledTimes(2);
      expect(callbackEnd).toBeCalledTimes(2);
    });
  });

  it('Turn page again when clicked after one page starting paged', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40} startPaged={true} onPaged={callbackEnd} onPagingStart={callbackStart}>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('0');
    expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
    wrapper.simulate('click');
    await waitExpectWithAct(() => {
      expect(wrapper.find(Floating).render().prop('x')).toBe('100');
      expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
    });
    wrapper.simulate('click');
    await waitExpectWithAct(() => {
      expect(wrapper.find(Floating).render().prop('x')).toBe('0');
      expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
      expect(callbackStart).toBeCalledTimes(2);
      expect(callbackEnd).toBeCalledTimes(2);
    });
  });

  it('Call AnimationBehaviorContext start and stop when paged', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider
        value={animationBehavior}>
        <Pageable pageWidth={40} id={'Pageable-test'}>
          <div>front</div>
          <div>back</div>
        </Pageable>
      </AnimationBehaviorContext.Provider>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('100');
    expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    expect(animationBehavior.start).toBeCalledTimes(1);
    expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Paging, 'Pageable-test'));
    expect(animationBehavior.stop).toBeCalledTimes(0);
    await waitExpectWithAct(() => {
      expect(wrapper.find(Floating).render().prop('x')).toBe('0');
      expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
      expect(animationBehavior.start).toBeCalledTimes(1);
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Paging, 'Pageable-test'));
      expect(animationBehavior.stop).toBeCalledTimes(1);
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Paging, 'Pageable-test'));
    });
  });

  it('Other events inside the page works correctly', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider
        value={animationBehavior}>
        <Pageable pageWidth={40} id={'Pageable-test'}>
          <Flippable id={'internal-Flippable-test'}>
            <div>front1</div>
            <div>back1</div>
          </Flippable>
          <div>back2</div>
        </Pageable>
      </AnimationBehaviorContext.Provider>,
    );
    expect(wrapper.find(Floating).render().prop('x')).toBe('100');
    expect(wrapper.find(Floating).render().children().first().html()).toBe('<div>front1</div>');
    wrapper.find('#internal-Flippable-test').simulate('click');
    expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Flip, 'internal-Flippable-test'));
    expect(animationBehavior.start).toBeCalledTimes(1);
    expect(animationBehavior.stop).toBeCalledTimes(0);
    await waitExpectWithAct(() => {
      expect(wrapper.find(Floating).render().prop('x')).toBe('100');
      expect(wrapper.find(Floating).render().children().first().html()).toBe('<div>back1</div>');
      expect(animationBehavior.start).toBeCalledWith(new RunningAnimation(AnimationType.Flip, 'internal-Flippable-test'));
      expect(animationBehavior.start).toBeCalledTimes(1);
      expect(animationBehavior.stop).toBeCalledWith(new RunningAnimation(AnimationType.Flip, 'internal-Flippable-test'));
      expect(animationBehavior.stop).toBeCalledTimes(1);
    });
  });

  it('Dont page if disabled by AnimationBehaviorContext', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      allowed: () => false,
      start: jest.fn(),
      stop: jest.fn(),
    };
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <Pageable id={'Pageable'} pageWidth={40}>
          <div>front</div>
          <div>back</div>
        </Pageable>
      </AnimationBehaviorContext.Provider>,
    );
    expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });
    expect(animationBehavior.start).toBeCalledTimes(0);
    expect(animationBehavior.stop).toBeCalledTimes(0);
  });

  it('Dont page with click when external pager is configured starting not paged', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };

    const ref = React.createRef();
    mount(<div ref={ref}/>);
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <Pageable id={'Pageable'} pageWidth={40} externalTrigger={ref}>
          <div>front</div>
          <div>back</div>
        </Pageable>
      </AnimationBehaviorContext.Provider>,
    );
    expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    await act(async () => {
      wrapper.simulate('click');
    });

    expect(animationBehavior.start).toBeCalledTimes(0);
    expect(animationBehavior.stop).toBeCalledTimes(0);
  });

  it('Dont page with click when external pager is configured starting not paged even with ref defined after Pageable', () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };

    const ref = React.createRef();
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <Pageable id={'Pageable'} pageWidth={40} externalTrigger={ref}>
          <div>front</div>
          <div>back</div>
        </Pageable>
      </AnimationBehaviorContext.Provider>,
    );
    mount(<div ref={ref}/>);
    expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');
    wrapper.simulate('click');

    expect(animationBehavior.start).toBeCalledTimes(0);
    expect(animationBehavior.stop).toBeCalledTimes(0);
  });

  it('Dont page with click when external pager is configured starting paged', async () => {
    const animationBehavior = {
      ...defaultAnimationBehavior,
      start: jest.fn(),
      stop: jest.fn(),
    };

    const ref = React.createRef();
    mount(<div ref={ref}/>);
    const wrapper = mount(
      <AnimationBehaviorContext.Provider value={animationBehavior}>
        <Pageable id={'Pageable'} pageWidth={40} startPaged={true} externalTrigger={ref}>
          <div>front</div>
          <div>back</div>
        </Pageable>
      </AnimationBehaviorContext.Provider>,
    );
    expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
    await act(async () => {
      wrapper.simulate('click');
    });

    expect(animationBehavior.start).toBeCalledTimes(0);
    expect(animationBehavior.stop).toBeCalledTimes(0);
  });

  it('Turn page when external pager is clicked', async () => {
    const callbackStart = jest.fn();
    const callbackEnd = jest.fn();

    const ref = React.createRef();
    mount(<div ref={ref}/>);
    const wrapper = mount(
      <Pageable id={'Pageable'} pageWidth={40} onPaged={callbackEnd} onPagingStart={callbackStart}
        externalTrigger={ref}>
        <div>front</div>
        <div>back</div>
      </Pageable>,
    );
    expect(wrapper.find(Floating).render().html()).toBe('<div>front</div>');

    await act(async () => {
      return ref.current.click();
    });

    await waitExpectWithAct(() => {
      expect(callbackStart).toBeCalledTimes(1);
      expect(callbackEnd).toBeCalledTimes(1);
      expect(wrapper.find(Floating).render().html()).toBe('<div>back</div>');
    });
  });
});
