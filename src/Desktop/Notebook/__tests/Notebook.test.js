import React from 'react';
import {shallow} from 'enzyme';
import Notebook from '../Notebook';
import {expectToThrow} from '../../../__tests/setupTests';
import Cover from '../Cover';
import Sheet from '../Sheet';
import PageableGroup from '../../Animations/PageableGroup';
import {PageType} from '../model';
import each from 'jest-each';

describe('<Notebook />', () => {
  it('Fails when content is not configured', () => {
    expectToThrow(() => shallow(<Notebook/>),
      'Warning: Failed prop type: The prop `content` is marked as required in `Notebook`, but its value is `undefined`');
  });

  each([
    [{other: 'other'}, 'content.name'],
    [{name: 'name', other: 'other'}, 'content.pages'],
    [{name: 'name', pages: [{}]}, 'content.pages[0].type'],
    [{name: 'name', pages: [{type: PageType.DIRECT}]}, 'content.pages[0].content'],
    [{name: 'name', pages: [{type: PageType.EXTERNAL}]}, 'content.pages[0].source'],
  ]).it('Fails with invalid content', (c, field) => {
    expectToThrow(() => shallow(<Notebook content={c}/>),
      `Warning: Failed prop type: The prop \`${field}\` is marked as required in \`Notebook\`, but its value is \`undefined\``);
  });

  each([
    [{name: 'name', pages: []}, 0],
    [{name: 'name', pages: [{type: PageType.DIRECT, content: 'c'}]}, 1],
  ]).it('Fails when content.pages is not minimum 4', (value, size) => {
    expectToThrow(() => shallow(<Notebook content={value}/>),
      `Warning: Failed prop type: The prop \`content.pagesSize\` should have min size 4 in \`Notebook\`. Size: ${size}`);
  });

  const content = {
    name: 'My lists',
    pages: [{type: PageType.DIRECT, content: 'front-1'},
      {type: PageType.DIRECT, content: 'front-2'},
      {type: PageType.DIRECT, content: '1'},
      {type: PageType.DIRECT, content: '2'},
      {type: PageType.EXTERNAL, source: '/'},
      {type: PageType.DIRECT, content: '4'},
      {type: PageType.DIRECT, content: 'back-1'},
      {type: PageType.DIRECT, content: 'back-2'}],
  };

  it('FrontCover should contain all pageable pages', () => {
    const wrapper = shallow(<Notebook content={content}/>);

    const pageable = wrapper.find(PageableGroup);
    expect(pageable.exists()).toBeTruthy();
    const pageables = pageable.children();
    expect(pageables.length).toBe(8);
    const cover1 = pageables.at(0).find(Cover);
    expect(cover1.exists()).toBeTruthy();
    expect(cover1.prop('content')).toMatchObject({content: 'front-1', type: PageType.DIRECT});
    const cover2 = pageables.at(1).find(Cover);
    expect(cover2.exists()).toBeTruthy();
    expect(cover2.prop('content')).toMatchObject({content: 'front-2', type: PageType.DIRECT});
    const page11 = pageables.at(2).find(Sheet);
    expect(page11.exists()).toBeTruthy();
    expect(page11.prop('content')).toMatchObject({content: '1', type: PageType.DIRECT});
    const page12 = pageables.at(3).find(Sheet);
    expect(page12.exists()).toBeTruthy();
    expect(page12.prop('content')).toMatchObject({content: '2', type: PageType.DIRECT});
    const page21 = pageables.at(4).find(Sheet);
    expect(page21.exists()).toBeTruthy();
    expect(page21.prop('content')).toMatchObject({source: '/', type: PageType.EXTERNAL});
    const page22 = pageables.at(5).find(Sheet);
    expect(page22.exists()).toBeTruthy();
    expect(page22.prop('content')).toMatchObject({content: '4', type: PageType.DIRECT});
    const back1 = pageables.at(6).find(Cover);
    expect(back1.exists()).toBeTruthy();
    expect(back1.prop('content')).toMatchObject({content: 'back-1', type: PageType.DIRECT});
    const back2 = pageables.at(7).find(Cover);
    expect(back2.exists()).toBeTruthy();
    expect(back2.prop('content')).toMatchObject({content: 'back-2', type: PageType.DIRECT});
  });

  it('back elements should have back styles', () => {
    const wrapper = shallow(<Notebook content={content}/>);

    const pageable = wrapper.find(PageableGroup);
    expect(pageable.exists()).toBeTruthy();
    const pageables = pageable.children();
    expect(pageables.length).toBe(8);
    const cover1 = pageables.at(0).find(Cover);
    expect(cover1.prop('back')).not.toBeTruthy();
    const cover2 = pageables.at(1).find(Cover);
    expect(cover2.exists()).toBeTruthy();
    expect(cover2.prop('back')).toBeTruthy();
    const page11 = pageables.at(2).find(Sheet);
    expect(page11.exists()).toBeTruthy();
    expect(page11.prop('back')).not.toBeTruthy();
    const page12 = pageables.at(3).find(Sheet);
    expect(page12.exists()).toBeTruthy();
    expect(page12.prop('back')).toBeTruthy();
    const page21 = pageables.at(4).find(Sheet);
    expect(page21.exists()).toBeTruthy();
    expect(page21.prop('back')).not.toBeTruthy();
    const page22 = pageables.at(5).find(Sheet);
    expect(page22.exists()).toBeTruthy();
    expect(page22.prop('back')).toBeTruthy();
    const back1 = pageables.at(6).find(Cover);
    expect(back1.exists()).toBeTruthy();
    expect(back1.prop('back')).not.toBeTruthy();
    const back2 = pageables.at(7).find(Cover);
    expect(back2.exists()).toBeTruthy();
    expect(back2.prop('back')).toBeTruthy();
  });
});
