import React from 'react';
import {mount} from 'enzyme';
import ComponentsAdministrationPage from './../ComponentsAdministrationPage';
import {mockedServer, rest, waitExpectWithAct} from '../../__tests/setupTests';
import BusinessCardView from '../BusinessCardView';
import ComponentButton from './../ComponentButton';
import {businessCardComponent} from './modelData';
import {Endpoints} from './../../endpoints';
import LockingError from '../../Common/Components/LockingError';
import '../../__tests/setupTests';
import 'jest-styled-components';
import {IconButton} from '@material-ui/core';
import ConfirmNotification from '../../Common/Components/ConfirmNotification';
import ErrorHandler from '../../Common/Components/ErrorHandler';

describe('<ComponentsAdministrationPage />', () => {
  it('Should be displayed correctly when there are not components', async () => {
    mockedServer.use(rest.get(Endpoints.components(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([]));
    }));

    const wrapper = mount(<ComponentsAdministrationPage/>);

    expect(wrapper).toBeTruthy();

    expect(wrapper.find('h1').text()).toBe('COMPONENTS ADMINISTRATOR');

    const createButton = wrapper.find('button');

    expect(createButton.text()).toBe('CREATE COMPONENT');

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(wrapper.update().find(ComponentButton).length).toBe(0);
      expect(mockedServer.receivedCalls()).toBe(1); // Get components
    });
  });

  it('Should be displayed correctly when there is only one component', async () => {
    mockedServer.use(rest.get(Endpoints.components(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([businessCardComponent]));
    }));

    const wrapper = mount(<ComponentsAdministrationPage/>);

    expect(wrapper).toBeTruthy();

    expect(wrapper.find('h1').text()).toBe('COMPONENTS ADMINISTRATOR');

    const createButton = wrapper.find('button');

    expect(createButton.text()).toBe('CREATE COMPONENT');

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(wrapper.update().find(ComponentButton).length).toBe(1);
      expect(wrapper.update().find(ComponentButton).at(0).text()).toContain(businessCardComponent.componentName);
      expect(wrapper.update().find(ComponentButton).at(0).text()).toContain(businessCardComponent.componentType);
      expect(mockedServer.receivedCalls()).toBe(1); // Get components
    });
  });

  it('Should be displayed correctly when there are multiple components', async () => {
    const notebookComponent = {
      componentName: 'Notebook Component',
      componentType: 'NOTEBOOK',
      componentId: 'someRandomId',
    };

    mockedServer.use(rest.get(Endpoints.components(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([businessCardComponent, notebookComponent]));
    }));

    const wrapper = mount(<ComponentsAdministrationPage/>);

    expect(wrapper).toBeTruthy();

    expect(wrapper.find('h1').text()).toBe('COMPONENTS ADMINISTRATOR');

    const createButton = wrapper.find('button');

    expect(createButton.text()).toBe('CREATE COMPONENT');

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(wrapper.update().find(ComponentButton).length).toBe(2);
      expect(wrapper.update().find(ComponentButton).at(0).text()).toContain(businessCardComponent.componentName);
      expect(wrapper.update().find(ComponentButton).at(0).text()).toContain(businessCardComponent.componentType);
      expect(wrapper.update().find(ComponentButton).at(1).text()).toContain(notebookComponent.componentName);
      expect(wrapper.update().find(ComponentButton).at(1).text()).toContain(notebookComponent.componentType);
      expect(mockedServer.receivedCalls()).toBe(1); // Get components
    });
  });

  it('Should fail when component retrieval fails', async () => {
    mockedServer.use(rest.get(Endpoints.components(), (req, res, ctx) => {
      return res.networkError('error message');
    }));

    const wrapper = mount(<ComponentsAdministrationPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(1);
      expect(wrapper.update().find(LockingError).text()).toContain('Network Error');
      expect(wrapper.update().find(LockingError).find('button').text()).toContain('Refresh');
    });
  });

  it('Should display BusnessCardView when clicks ComponentButton', async () => {
    mockedServer.use(rest.get(Endpoints.components(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([businessCardComponent]));
    }));
    const wrapper = mount(<ComponentsAdministrationPage/>);

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(1);
    });

    expect(wrapper.text()).toContain('Welcome, Select an item to edit it');
    expect(wrapper.find(BusinessCardView).length).toBe(0);

    await waitExpectWithAct(() => {
      const selectors = wrapper.update().find(ComponentButton);
      expect(selectors.length).toBe(1);
      selectors.at(0).find(ComponentButton).at(0).simulate('click');
      expect(wrapper.update().find(BusinessCardView).length).toBe(1);
    });
  });

  it('Should delete the component on clicks delete component button', async () => {
    const notebookComponent = {
      componentName: 'Notebook Component',
      componentType: 'NOTEBOOK',
      componentId: 'someRandomNotebookId',
    };

    mockedServer.use(rest.get(Endpoints.components(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([businessCardComponent, notebookComponent]));
    }));

    mockedServer.use(rest.delete(Endpoints.deleteComponent(businessCardComponent.componentId), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json(notebookComponent));
    }));

    const wrapper = mount(<ComponentsAdministrationPage/>);

    expect(wrapper).toBeTruthy();

    expect(wrapper.find('h1').text()).toBe('COMPONENTS ADMINISTRATOR');

    const createButton = wrapper.find('button');

    expect(createButton.text()).toBe('CREATE COMPONENT');

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(wrapper.update().find(ComponentButton).length).toBe(2);
      expect(wrapper.update().find(ComponentButton).at(0).text()).toContain(businessCardComponent.componentName);
      expect(wrapper.update().find(ComponentButton).at(0).text()).toContain(businessCardComponent.componentType);
      expect(wrapper.update().find(ComponentButton).at(1).text()).toContain(notebookComponent.componentName);
      expect(wrapper.update().find(ComponentButton).at(1).text()).toContain(notebookComponent.componentType);
    });

    wrapper.find(ComponentButton).at(1).simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(BusinessCardView).length).toBe(1);
    });

    wrapper.find(IconButton).at(0).simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(ConfirmNotification).length).toBe(1);
    });

    wrapper.find('button').at(1).simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(3); // Get components, Delete Component, Refresh retrieve
    });
  });

  it('Should not delete the component when cancel delete is clicked', async () => {
    const notebookComponent = {
      componentName: 'Notebook Component',
      componentType: 'NOTEBOOK',
      componentId: 'someRandomNotebookId',
    };

    mockedServer.use(rest.get(Endpoints.components(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([businessCardComponent, notebookComponent]));
    }));

    const wrapper = mount(<ComponentsAdministrationPage/>);

    expect(wrapper).toBeTruthy();

    expect(wrapper.find('h1').text()).toBe('COMPONENTS ADMINISTRATOR');

    const createButton = wrapper.find('button');

    expect(createButton.text()).toBe('CREATE COMPONENT');

    await waitExpectWithAct(() => {
      expect(wrapper.update()).toBeTruthy();
      expect(wrapper.update().find(ComponentButton).length).toBe(2);
      expect(wrapper.update().find(ComponentButton).at(0).text()).toContain(businessCardComponent.componentName);
      expect(wrapper.update().find(ComponentButton).at(0).text()).toContain(businessCardComponent.componentType);
      expect(wrapper.update().find(ComponentButton).at(1).text()).toContain(notebookComponent.componentName);
      expect(wrapper.update().find(ComponentButton).at(1).text()).toContain(notebookComponent.componentType);
    });

    wrapper.find(ComponentButton).at(1).simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(BusinessCardView).length).toBe(1);
    });

    wrapper.find(IconButton).at(0).simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(ConfirmNotification).length).toBe(1);
    });

    wrapper.find('button').at(0).simulate('click');

    await waitExpectWithAct(() => {
      expect(wrapper.update().find(ComponentButton).length).toBe(2);
      expect(mockedServer.receivedCalls()).toBe(1); // Get components
    });
  });

  it('Should show error when delete component fails', async () => {
    const notebookComponent = {
      componentName: 'Notebook Component',
      componentType: 'NOTEBOOK',
      componentId: 'someRandomNotebookId',
    };

    mockedServer.use(rest.get(Endpoints.components(), (req, res, ctx) => {
      return res(ctx.status(200), ctx.json([businessCardComponent, notebookComponent]));
    }));

    mockedServer.use(rest.delete(Endpoints.deleteComponent(businessCardComponent.componentId)), (req, res, ctx) => {
      return res.networkError('error message');
    });

    const wrapper = mount(<ComponentsAdministrationPage/>);

    await waitExpectWithAct(() => {
      wrapper.update().find('button').at(1);
      wrapper.update().find(ComponentButton).at(1).simulate('click');
      expect(wrapper.update().find(BusinessCardView).length).toBe(1);
      wrapper.update().find(IconButton).at(0).simulate('click');
      expect(wrapper.update().find(ConfirmNotification).length).toBe(1);
    });

    wrapper.find('button').at(1).simulate('click');

    await waitExpectWithAct(() => {
      expect(mockedServer.receivedCalls()).toBe(2);
      expect(wrapper.update().find(ErrorHandler).text()).toContain('Network Error');
      expect(wrapper.update().find(ErrorHandler).find('button').text()).toContain('Refresh');
    });
  });
});
