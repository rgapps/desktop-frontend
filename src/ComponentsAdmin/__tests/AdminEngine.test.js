import React from 'react';
import '../../__tests/setupTests';
import {expectToThrow} from '../../__tests/setupTests';
import {shallow} from 'enzyme';
import {AdminContextProvider} from '../AdminEngine';

describe('<AdminContextProvider />', () => {
  it('Should fail if no child is given', () => {
    expectToThrow(() => shallow(<AdminContextProvider/>),
      'Warning: Failed prop type: `AdminContextProvider` should contain one children (0 received)');
  });
});
